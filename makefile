SOURCE=src
GLADMAN_AES_SOURCE=src/aes_implementations/brian_gladman
GLADMAN_HMAC_SOURCE=src/hmac_implementations/brian_gladman
DESTINATION=bin
EXECUTABLE=crips

all: tests
	gcc -Wall -o "${DESTINATION}/${EXECUTABLE}" ${SOURCE}/crips_runner.c ${SOURCE}/crips.c ${SOURCE}/crypt_interface.c ${SOURCE}/crips_options.c ${SOURCE}/string_ops.c ${SOURCE}/file_ops.c ${SOURCE}/help.c ${SOURCE}/system_ops.c ${GLADMAN_AES_SOURCE}/libaes.a ${GLADMAN_HMAC_SOURCE}/libhmac.a ${SOURCE}/base36.o
	cd ${SOURCE}; rm *.o

tests: bin_directory
	gcc -Wall -o "${DESTINATION}/test_base36" ${SOURCE}/tests/test_base36.c ${SOURCE}/base36.o ${SOURCE}/file_ops.c ${SOURCE}/string_ops.c ${SOURCE}/crips_options.c ${SOURCE}/help.c ${SOURCE}/system_ops.c
	gcc -Wall -o "${DESTINATION}/test_crips" ${SOURCE}/tests/test_crips.c ${SOURCE}/crips.c ${SOURCE}/crypt_interface.c ${SOURCE}/crips_options.c ${SOURCE}/file_ops.c ${SOURCE}/help.c ${SOURCE}/system_ops.c ${GLADMAN_AES_SOURCE}/libaes.a ${GLADMAN_HMAC_SOURCE}/libhmac.a ${SOURCE}/string_ops.c ${SOURCE}/base36.o
	./bin/test_base36
	./bin/test_crips
	@echo "All tests passed"

bin_directory: base36
	test -d ${DESTINATION} || mkdir ${DESTINATION}

base36: aes_archive hmac_archive
	cd ${SOURCE}; gcc -Wall -c base36.c

hmac_archive: hmac_lib
	cd ${GLADMAN_HMAC_SOURCE}; ar rcs libhmac.a *.o; rm *.o

hmac_lib:
	cd ${GLADMAN_HMAC_SOURCE}; gcc -c -O2 -fomit-frame-pointer *.c

aes_archive: aes_lib
	cd ${GLADMAN_AES_SOURCE}; ar rcs libaes.a *.o; rm *.o

aes_lib:
	cd ${GLADMAN_AES_SOURCE}; gcc -c -O2 -fomit-frame-pointer aescrypt.c aeskey.c aestab.c aes_modes.c

clean:
	rm ${DESTINATION}/${EXECUTABLE}
	rm ${GLADMAN_AES_SOURCE}/libaes.a

