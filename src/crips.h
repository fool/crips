/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "crypt_interface.h"
#include "crips_options.h"
#define CRIPS_VERSION "0.8"

#define OUTFILE_MODE_PRESERVE_NAME 1
#define OUTFILE_MODE_NAMED_FILE 2

#define ERROR_NONE 0
#define ERROR_BAD_INFILE 1
#define ERROR_BAD_OUTFILE 2
#define ERROR_BAD_KEYFILE 3
#define ERROR_MISSING_MODE 4
#define ERROR_MISSING_INFILE 5
#define ERROR_MISSING_OUTFILE 6
#define ERROR_UNREADABLE_INFILE 7
#define ERROR_UNWRITABLE_OUTFILE 8
#define ERROR_UNREADABLE_KEYFILE 9
#define ERROR_BAD_INITIALIZATION_VECTOR 10
#define ERROR_BAD_RANDOMIZER 11
#define ERROR_GENERATING_INITIALIZATION_VECTOR 12
#define ERROR_BAD_HMAC 13
#define ERROR_OUT_OF_MEMORY 100

int crips_start_command_line(int argc, char **argv);
int crips_start(Crips_Options *options);
void crips_encrypt_file(Crips_Options *options);
int crips_decrypt_file(Crips_Options *options);

