#include <stdio.h>
#include <string.h>
#include "help.h"
#include "crips.h"
#include "system_ops.h"

/**
 * Prints all the crips_exit status codes and their meanings, then crips_exits the program.
 */
void explain_errors() {
	printf("Exit Status Number\tMeaning\n");
	printf("\t%d\t\tNo error occurred\n", ERROR_NONE);
	printf("\t%d\t\tThe infile argument supplied was invalid\n", ERROR_BAD_INFILE);
	printf("\t%d\t\tThe outfile argument supplied was invalid\n", ERROR_BAD_OUTFILE);
	printf("\t%d\t\tThe keyfile argument supplied was invalid\n", ERROR_BAD_KEYFILE);
	printf("\t%d\t\tThe iv argument supplied was invalid\n", ERROR_BAD_INITIALIZATION_VECTOR);
	printf("\t%d\t\tThe ranomizer argument supplied was invalid\n", ERROR_BAD_RANDOMIZER);
	printf("\t%d\t\tYou did not provide a mode, encrypt or decrypt\n", ERROR_MISSING_MODE);
	printf("\t%d\t\tYou did not provide an infile\n", ERROR_MISSING_INFILE);
	printf("\t%d\t\tYou did not provide an outfile\n", ERROR_MISSING_OUTFILE);
	printf("\t%d\t\tCould not open the infile for reading\n", ERROR_UNREADABLE_INFILE);
	printf("\t%d\t\tCould not open the outfile for writing\n", ERROR_UNWRITABLE_OUTFILE);
	printf("\t%d\t\tCould not open the keyfile for reading\n", ERROR_UNREADABLE_KEYFILE);
	printf("\t%d\t\tCould not generate an initialization vector randomly\n", ERROR_GENERATING_INITIALIZATION_VECTOR);
	printf("\t%d\t\tDuring decryption the HMAC found in the input file did not match the\n", ERROR_BAD_HMAC);
	printf("\t\t\tone produced by the encrypted data.\n");
	printf("\t%d\t\tOut of memory\n", ERROR_OUT_OF_MEMORY);
	crips_exit(ERROR_NONE);
}

/**
 * Prints help and usage for crips with an optional error message, then crips_exits the program.
 *
 * param error_message A message to display
 * param crips_exit_code The crips_exit status code to crips_exit with
 */
void crips_help(const char *error_message, int crips_exit_code) {
	if (strlen(error_message) > 0) {
		fprintf(stderr, "ERROR: %s\n\n\n", error_message);
	}
	printf("Usage: crips [encryption mode] [infile] [outfile mode] [options]\n");
	printf("Version: %s\n\n", CRIPS_VERSION);
	printf(" Encyption modes:\n");
	printf("  -e,  --encrypt          Encrypt\n");
	printf("  -d,  --decrypt          Decrypt\n");
	printf("\n");
	printf(" Infile:\n");
	printf("  -i,  --infile <file>    The file for encryption or decryption\n");
	printf("\n");
	printf(" Outfile modes:\n");
	printf("  -o,  --outfile <file>   The file to write after encryption or decryption\n");
	printf("  -p,  --preserve-name    Keep the same filename. In encryption mode, this will encrypt\n");
	printf("                          the filename and base64 encode it. In decrption mode this will\n");
	printf("                          base64 decode the filename and unencrypt it\n");
	printf("\n");
	printf(" Optional arguments\n");
	printf("  -k,  --keyfile <file>   File that contains the key (default key used if not provided)\n");
	printf("  -h,  --hmac-keyfile <file> File that contains the hmac key (default hmac key used if not provided)\n");
	printf("  -iv, --iv <vector>      You can pass in your own initialization vector. By default an \n");
	printf("                          IV is randomly generated on each program execution. The given IV\n");
	printf("                          needs to be a single string of 16 positive integers each separated\n");
	printf("                          by a space. The value for any given integer is 0 to 255 inclusive.\n");
	printf("  -r, --randomizer <name> If an IV is not provided, one is generated randomly. The source of\n");
	printf("                          the random numbers can either be \"random\" or \"urandom\" which\n");
	printf("                          read from /dev/random and /dev/urandom respectively. This option\n");
	printf("                          defaults to \"urandom\"\n");
	printf("  -h,  --help             Show this message\n");
	printf("  -v,  --verbose          Write some more stuff to terminal\n");
	printf("  -?,  --errors           Print detailed info about what the crips_exit status codes mean\n");
	printf("\n");
	crips_exit(crips_exit_code);
}



