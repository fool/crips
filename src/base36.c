/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <strings.h>
#include <stdlib.h>
#include "string_ops.h"
#include "base36.h"

// Jordan Sterling

char *base_36_encode_uint(unsigned int *bytes_to_encode, int length)
{
	int i;
	int j;
	unsigned int num;
	char bytes_encoded[MAX_BASE36_CHARS_OF_ENCODED_INT];

	char *encoded_bytes = malloc((sizeof(char) * length * MAX_BASE36_CHARS_OF_ENCODED_INT) + 1); // MAX_BASE36_CHARS_OF_ENCODED_INT chars per int. + 1 null
	int encoded_bytes_index = 0;
	for (i = 0; i < length; i++) {
		num = bytes_to_encode[i];

		// for each int we receive, turn it int a MAX_BASE36_CHARS_OF_ENCODED_INT character string
		// then append it to our master string
		encode_int(num, bytes_encoded, MAX_BASE36_CHARS_OF_ENCODED_INT);
		for (j = 0; j < MAX_BASE36_CHARS_OF_ENCODED_INT; j++) {
			encoded_bytes[encoded_bytes_index++] = bytes_encoded[j];
		}
	}
	encoded_bytes[encoded_bytes_index] = '\0';

	return encoded_bytes;
}

/**
 * The value and the length
 */
char *base_36_encode_uchar(unsigned char *bytes_to_encode, int length)
{
	int i;
	unsigned int num;
	char bytes_encoded[MAX_BASE36_CHARS_OF_ENCODED_CHAR];
	int encoded_bytes_index = 0;
	char *encoded_bytes = malloc((sizeof(char) * length * MAX_BASE36_CHARS_OF_ENCODED_CHAR) + 1); // MAX_BASE36_CHARS_OF_ENCODED_CHAR chars per char. + 1 null

	for (i = 0; i < length; i++)
	{
		num = (unsigned int) bytes_to_encode[i];

		encode_int(num, bytes_encoded, MAX_BASE36_CHARS_OF_ENCODED_CHAR);
		encoded_bytes[encoded_bytes_index++] = bytes_encoded[0];
		encoded_bytes[encoded_bytes_index++] = bytes_encoded[1];
	}
	encoded_bytes[encoded_bytes_index] = '\0';

	return encoded_bytes;
}

/**
 * Turns an unsigned int into a 7 character base36 representation
 */
void encode_int(unsigned int value, char encoded_value[], int length)
{
	const char *charmap = "0123456789abcdefghijklmnopqrstuvwxyz";
	unsigned int chunk;
	int i;

	// MAX_BASE36_CHARS_OF_ENCODED_INT is the max.
	// have to build this RIGHT to LEFT to keep it littl endian
	for (i = 0; i < length; i++) {
		chunk = value % 36;
		encoded_value[length - i - 1] = charmap[chunk];
		value /= 36;
	}
}

/**
 * Decodes a string to unsigned int array.
 * The string should always be length 8, this will look at the first 8 chars.
 * The decoded value is an unsigned int.
 */
unsigned int decode_bytes_int(char *encoded_value)
{
	int i;
	unsigned int char_value = 0;
	unsigned int value = 0;

	
	// offset for the encoded bytes
	unsigned int bases[8] = {
		0,          // 36 ** 7, too big and not necessary
		2176782336, // 36 ** 6
		60466176,   // 36 ** 5
		1679616,    // 36 ** 4
		46656,      // 36 ** 3
		1296,       // 36 ** 2
		36,         // 36 ** 1
		1,          // 36 ** 0
	};

	for (i = 0; i < MAX_BASE36_CHARS_OF_ENCODED_INT; i++) {
		char_value = reverse_map(encoded_value[i]);// * 36;

		// you only get the base if you actually had something
		if (char_value > 0) {
			char_value *= bases[i];
		}

		// add on to value
		value += char_value;
	}
	return value;
}

/**
 * Decodes a string to unsigned char array.
 * The string should have length 2 always, no matter what this will look at the first 2 chars.
 * The decoded value is always a single unsigned char.
 */
unsigned char decode_bytes_char(char *encoded_value)
{
	unsigned char char_value = 0;

	char_value  = reverse_map(encoded_value[0]);
	if (char_value > 0) {
		char_value *= 36;
	}
	char_value += reverse_map(encoded_value[1]);

	return char_value;
}

/**
 * Map of base36 character to the integer value it represents in ascii.
 *
 * i think this pretty close to atoi?
 */
unsigned int reverse_map(char encoded_value)
{
	unsigned int char_value = 0;

	if (encoded_value < 36) {
		return (unsigned int) encoded_value;
	}

	if (encoded_value >= 48 && encoded_value <= 57) {
		char_value = encoded_value - 48; // maps numbers, so "0" => 0
	}
	else {
		char_value = encoded_value - 87; // so a => 10
	}
	return char_value;
}

// check limits
unsigned int *base_36_decode_uint(char *encoded_bytes, int length)
{
	unsigned int expected_decoded_length = length / MAX_BASE36_CHARS_OF_ENCODED_INT;
	unsigned int digit;
	unsigned int decoded_bytes_index = 0;
	unsigned int *decoded_bytes = malloc(sizeof(unsigned int) * expected_decoded_length);


	for (digit = 0; digit < length; digit += MAX_BASE36_CHARS_OF_ENCODED_INT) {
		if (!is_text_valid_base36_uint(&encoded_bytes[digit], MAX_BASE36_CHARS_OF_ENCODED_INT)) {
			free(decoded_bytes);
			return NULL;
		}
		decoded_bytes[decoded_bytes_index++] = decode_bytes_int(&encoded_bytes[digit]);
	}
	return decoded_bytes;
}

unsigned char *base_36_decode_uchar(char *encoded_bytes, int length)
{
	unsigned int expected_decoded_length = length / MAX_BASE36_CHARS_OF_ENCODED_CHAR;
	unsigned int i;
	unsigned int decoded_bytes_index = 0;
	unsigned char *decoded_bytes = malloc(sizeof(unsigned char) * (expected_decoded_length + 1));


	for (i = 0; i < length; i += MAX_BASE36_CHARS_OF_ENCODED_CHAR) {
		decoded_bytes[decoded_bytes_index++] = decode_bytes_char(&encoded_bytes[i]);
	}
	decoded_bytes[decoded_bytes_index] = '\0';
	return decoded_bytes;
}


/**
 * Detects if the given 8 asci characters could represent a decoded base36 unsigned integer.
 * This limit comes from Base36(UINT_MAX) = 01Z141Z3
 *
 * So we peel off the characters and if they are above the max limit for base36 it is rejected.
 * other invalid strings are rejected too, like not being length 8;
 *
 *
 */
int is_text_valid_base36_uint(char * number, int length)
{
	const int Z = 122;
	const int ZERO = 48;
	const int ONE = 49;
	const int THREE = 51;
	const int FOUR = 52;

	if (length != MAX_BASE36_CHARS_OF_ENCODED_INT) {
		return 0;
	}

	if (number[0] > ZERO || number[1] > ONE) {
		return 0;
	}
	else if(number[1] == ONE) {
		if (number[2] == Z) {
			if (number[3] > ONE) {
				return 0;
			}
			else if (number[3] == ONE) {
				if (number[4] > FOUR) {
					return 0;
				}
				else if (number[4] == FOUR) {
					if (number[5] > ONE) {
						return 0;
					}
					else if(number[5] == ONE) {
						if (number[6] == Z) {
							if (number[7] > THREE) {
								return 0;
							}
						}
					}
				}
			}
		}
	}
	return 1;
}
