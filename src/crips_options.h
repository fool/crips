#include "crypt_interface.h"

struct Crips_Options_Definition {
	int mode;
	char *infile;
	char *outfile;
	char *random_source;
	unsigned char initialization_vector[INITIALIZATION_VECTOR_LENGTH];
	unsigned char *aes_key;
	unsigned char *hmac_key;
	int outfile_mode;
	int initialization_vector_provided;
	int verbose;
	
};

typedef struct Crips_Options_Definition Crips_Options;

Crips_Options *crips_options_parse(int argc, char **argv);
Crips_Options *crips_options_new();


