#include <stdio.h>
#include <stdlib.h>
#include "crips.h"

void crips_exit(int exit_code) {
	fflush(stdout);
	exit(exit_code);
}

/**
 * global malloc wrapper.
 */
void *Malloc(size_t size) {
	void *memory = malloc(size);
	if (!memory) {
		fprintf(stderr, "Out of memory, tried to allocate %zu\n", size);
		crips_exit(ERROR_OUT_OF_MEMORY);
	}
	return memory;
}

