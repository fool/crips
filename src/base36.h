/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/**
 * Output buffer size is determined by:
 * possible values / total letters in alphabet = log(2**32) / log(36) =  32/log(36)  => 6.9
 *
 * There is a max of 7 chars, but we use 8 anyway to be compatiable with char.
 */
#define MAX_BASE36_CHARS_OF_ENCODED_INT 8
#define MAX_BASE36_CHARS_OF_ENCODED_CHAR 2




void encode_int(unsigned int value, char encoded_value[], int length);


char *base_36_encode_uint(unsigned int *bytes_to_encode, int length);
char *base_36_encode_uchar(unsigned char *bytes_to_encode, int length);
unsigned int *base_36_decode_uint(char *encoded_bytes, int length);
unsigned char *base_36_decode_uchar(char *encoded_bytes, int length);


unsigned int decode_bytes_int(char *encoded_value);
unsigned char decode_bytes_char(char *encoded_value);
unsigned int reverse_map(char encoded_value);
int is_text_valid_base36_uint(char * number, int length);
