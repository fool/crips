/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <string.h>
#include "crips.h"
#include "crypt_interface.h"
#include "system_ops.h"
#define BUFFER_LENGTH 200

/**
 * Interface to brian gladman implementation from user h4rdc0ded in this post:
 * http://forums.devshed.com/c-programming-42/aes-encrypt-decrypt-in-c-687368.html
 */



int min(int a, int b)
{
	return a < b ? a : b;
}


/**
 * Initializes the aes code and seeds rand with the current time.
 * Initialization vectors are genated based on rand() calls, which
 * sucks.
 */
void crypt_init()
{
	aes_init();
}

/**
 * Generates IV. You give a buffer, it will fill it for you.
 *
 * param iv_buffer The buffer to be filled with an initialization vector
 * param length    How big the buffer is
 * return 1 on success, 0 on failure
 */
int generate_initialization_vector(unsigned char *iv_buffer, int length, const char *random_source)
{
	FILE *random = fopen(random_source, "r");
	if (!random) {
		return 0;
	}
	int bytes_read = fread(iv_buffer, sizeof(char), length, random);
	if (bytes_read != length) {
		return 0;
	}
	int error = fclose(random);
	if (error == EOF) {
		return 0;
	}
	return 1;
}

/**
 * Reads an initialization vector from an encrypted file.
 * When the files are written, the first 16 bytes are the initialization
 * vector. Your FILE pointer should already be at the start of the file
 *
 * param encrypted_file Open file handle to the encrpted file
 * param initialization_vector A buffer to fill with the vector's contents
 * return Whether or not the initialization vector was read successfully
 */
int read_encrypted_file_metadata(FILE *encrypted_file, unsigned char *initialization_vector, unsigned char *hmac)
{
	if (fread(hmac, 1, HMAC_LENGTH, encrypted_file) < HMAC_LENGTH) {
		return 0;
	}
	if (fread(initialization_vector, 1, INITIALIZATION_VECTOR_LENGTH, encrypted_file) < INITIALIZATION_VECTOR_LENGTH) {
		return 0;
	}
	return 1;
}

/**
 * Uses a key provided and encrypts/decrypts the file contents. Since this is using
 * AES symmetric encryption, if it is encrypted the contents will be decrypted and vice
 * versa.
 *
 * If this is used for decryption, and you used the encryption scheme above, your inFile
 * ptr should be advanced to the 16th byte already. There is no need to re-read the first
 * 16 bytes of the file because they were not actually part of your original contents.
 *
 * The hmac is always returned and will be the same in the encrypted and decrypted cases.
 * The hmac is consists of hmac key + iv + cipher text
 *
 * param inFile The file to read file
 * param outFile The file to write the output to
 * param aes_key The 256bit AES key
 * param hmac_key The 256big HMAC key
 * param iv The initialization vector
 * return hmac of the encrypted text
 */
unsigned char * encrypt_or_decrypt_file(int mode, FILE *inFile, FILE *outFile, const unsigned char *aes_key, const unsigned char *hmac_key, unsigned char *iv)
{
	int chars_read;
	hmac_ctx hmac_context[1];
	aes_encrypt_ctx aes_context[1];
	unsigned char in_buffer[BUFFER_LENGTH];
	unsigned char out_buffer[BUFFER_LENGTH];

	unsigned char *hmac = (unsigned char *)Malloc(sizeof(unsigned char) * HMAC_LENGTH);

    hmac_sha_begin(hmac_context);
    hmac_sha_key(hmac_key, AES_KEY_LENGTH, hmac_context); // hmac key
	hmac_sha_data(iv, INITIALIZATION_VECTOR_LENGTH, hmac_context); // initialization vector

	aes_encrypt_key256(aes_key, aes_context);
	while((chars_read = fread(in_buffer, 1, sizeof(in_buffer), inFile)) > 0) {
		if (mode == MODE_ENCRYPT) {
			aes_cfb_encrypt(in_buffer, out_buffer, chars_read, iv, aes_context);
			hmac_sha_data(out_buffer, chars_read, hmac_context);
		}
		else {
			aes_cfb_decrypt(in_buffer, out_buffer, chars_read, iv, aes_context);
			hmac_sha_data(in_buffer, chars_read, hmac_context);
		}
		fwrite(out_buffer, 1, chars_read, outFile);
	}
	// this will fill hmac with the resulting hmac
    hmac_sha_end(hmac, HMAC_LENGTH, hmac_context);
	return hmac;

	// now that you have hmac, you return it
	//return the hmac?
	//the caller wil lneed to know what to do with it.
	//either compare to orignal, or write it to file, or whatever.
	//
	// so that's the interface change
	// this now accepts hmac key, hmac key len, and returns hmac of the contents. 
	// the hmac only ever refers to the mac result of 
	// hmac = hmac ( hmac key + iv + encrypted text)
	// if its encryption or decryption, you should get the same hmac both times.
}

/**
 * The returned char * must be freed at some point.
 *
 * param string The string to decrypt/encrypt
 * param string_length How long "string" is
 * param key The key to decrypt/encrypt with, AES 256b
 * param iv The initialization vector to decrypt/encrypt with
 * return The resultant text in an unsigned char array. You must free this memory
 */
unsigned char *encrypt_or_decrypt_string(int mode, unsigned char *string, int string_length, const unsigned char *key, unsigned char *iv)
{
	int string_index;
	int i;
	int chars_to_copy;
	aes_encrypt_ctx aes_context[1];

	unsigned char *encrypted_string = malloc(sizeof(unsigned char) * (string_length + 1));
	unsigned char in_buffer[BUFFER_LENGTH];
	unsigned char out_buffer[BUFFER_LENGTH];
	memset(in_buffer, 0, BUFFER_LENGTH);
	memset(out_buffer, 0, BUFFER_LENGTH);

	// need a copy of the iv because it will be modified by the AES functions
	// The iv should not be changed in this function
	unsigned char iv_copy[INITIALIZATION_VECTOR_LENGTH];
	memcpy(iv_copy, iv, INITIALIZATION_VECTOR_LENGTH);
 
	aes_encrypt_key256(key, aes_context);
	for (string_index = 0; string_index < string_length; ) // string_index is incremented inside the loop
	{
		// take the first BUFFER_LENGTH chars from string, stick into the buffers
		chars_to_copy = min(string_length - string_index, BUFFER_LENGTH);
		memcpy(in_buffer, &string[string_index], chars_to_copy);
		if (mode == MODE_ENCRYPT) {
			aes_cfb_encrypt(in_buffer, out_buffer, chars_to_copy, iv_copy, aes_context);
		}
		else {
			aes_cfb_decrypt(in_buffer, out_buffer, chars_to_copy, iv_copy, aes_context);
		}

		// copy all the bits to the output string
		for (i = 0; i < chars_to_copy; i++) {
			encrypted_string[string_index++] = out_buffer[i];
		}
		memset(in_buffer, 0, BUFFER_LENGTH);
		memset(out_buffer, 0, BUFFER_LENGTH);
	}
	encrypted_string[i] = '\0';
	return encrypted_string;
}
