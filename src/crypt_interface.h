/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include "aes_implementations/brian_gladman/aes.h"
#include "hmac_implementations/brian_gladman/hmac.h"

// the gladman code relies on this length being 16.. do not touch
#define INITIALIZATION_VECTOR_LENGTH 16

// only sha256 is supported for hmac
#define HMAC_LENGTH 32

// only 256bit keys are used. this is for both the encryption and hmac
#define AES_KEY_LENGTH 32


#define MODE_ENCRYPT 1
#define MODE_DECRYPT 2

#define MODE_RANDOM "/dev/random"
#define MODE_URANDOM "/dev/urandom"

unsigned char *encrypt_or_decrypt_string(int mode, unsigned char *string, int string_length, const unsigned char *key, unsigned char *iv);
unsigned char *encrypt_or_decrypt_file(int mode, FILE *fileIn, FILE *fileOut, const unsigned char *aes_key, const unsigned char *hmac_key, unsigned char *iv);

int generate_initialization_vector(unsigned char *iv_buffer, int length, const char *random_source);
int read_encrypted_file_metadata(FILE *encrypted_file, unsigned char *initialization_vector, unsigned char *hmac);

void crypt_init();


