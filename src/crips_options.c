#include <string.h>
#include "crypt_interface.h"
#include "crips.h"
#include "help.h"
#include "string_ops.h"
#include "system_ops.h"
#include "file_ops.h"

Crips_Options *crips_options_new() {
	Crips_Options *options;
	options = (Crips_Options *)Malloc(sizeof(Crips_Options));
	options->random_source = MODE_RANDOM;
	options->outfile_mode = 0;
	options->initialization_vector_provided = 0;
	options->verbose = 0;

	// replace with your own key here
	options->aes_key = (unsigned char *) "42AB7FCE7BFEEE03E16719044916CBD475F6D000F230D213FF0F4775EF8D46F5";

	// replace with your own key here
	options->hmac_key = (unsigned char *) "BC5E4B9BF20005092D2DA597151581AE348256443626244FC652C71E86D539E0";
	return options;
}


/**
 * Parses the command line arguments
 */
Crips_Options *crips_options_parse(int argc, char **argv) {

	int mode_selected = 0;
	int infile_selected = 0;

	if (argc == 1) {
		crips_help("", ERROR_NONE);
	}

	//Crips_Options *options = malloc(sizeof(Crips_Options *));
	Crips_Options *options = crips_options_new();
	int i = 1;
	while (i < argc) {
		if (strcmp("-e", argv[i]) == 0 || strcmp("--encrypt", argv[i]) == 0) {
			mode_selected = 1;
			options->mode = MODE_ENCRYPT;
		}
		else if (strcmp("-d", argv[i]) == 0 || strcmp("--decrypt", argv[i]) == 0) {
			mode_selected = 1;
			options->mode = MODE_DECRYPT;
		}
		else if (strcmp("-v", argv[i]) == 0 || strcmp("--verbose", argv[i]) == 0) {
			options->verbose = 1;
		}
		else if (strcmp("-h", argv[i]) == 0 || strcmp("--help", argv[i]) == 0) {
			crips_help("", ERROR_NONE);
		}
		else if (strcmp("-?", argv[i]) == 0 || strcmp("--errors", argv[i]) == 0) {
			explain_errors();
		}
		else if (strcmp("-i", argv[i]) == 0 || strcmp("--infile", argv[i]) == 0) {
			i++;
			if (i >= argc)
			{
				crips_help("You didn't give an infile... wtf m8", ERROR_BAD_INFILE);
			}
			infile_selected = 1;
			options->infile = argv[i];
		}
		else if (strcmp("-o", argv[i]) == 0 || strcmp("--outfile", argv[i]) == 0) {
			i++;
			if (i >= argc) {
				crips_help("You didn't give an outfile... wtf m8", ERROR_BAD_OUTFILE);
			}
			options->outfile_mode = OUTFILE_MODE_NAMED_FILE;
			options->outfile = argv[i];
		}
		else if (strcmp("-p", argv[i]) == 0 || strcmp("--preserve-name", argv[i]) == 0) {
			options->outfile_mode = OUTFILE_MODE_PRESERVE_NAME;
		}
		else if (strcmp("-k", argv[i]) == 0 || strcmp("--keyfile", argv[i]) == 0) {
			i++;
			if (i >= argc) {
				crips_help("You didn't give a keyfile... wtf m8", ERROR_BAD_KEYFILE);
			}
			char *keyfile = argv[i];

			// what key to use?
			char *aes_key = read_file_to_string(keyfile);
			if (!aes_key) {
				fprintf(stderr, "ERROR: Unable to read keyfile: %s\n", keyfile);
				crips_exit(ERROR_UNREADABLE_KEYFILE);
			}
			
			options->aes_key = (unsigned char *)aes_key;
		}
		else if (strcmp("-h", argv[i]) == 0 || strcmp("--hmac-keyfile", argv[i]) == 0) {
			i++;
			if (i >= argc) {
				crips_help("You didn't give an hmac keyfile... wtf m8", ERROR_BAD_KEYFILE);
			}
			char *hmac_keyfile = argv[i];

			// what key to use?
			char *hmac_key = read_file_to_string(hmac_keyfile);
			if (!hmac_key) {
				fprintf(stderr, "ERROR: Unable to read hmac keyfile: %s\n", hmac_keyfile);
				crips_exit(ERROR_UNREADABLE_KEYFILE);
			}
			
			options->hmac_key = (unsigned char *)hmac_key;
		}
		else if ((strcmp("-iv", argv[i]) == 0 || strcmp("--iv", argv[i]) == 0) && !options->initialization_vector_provided) {
			i++;
			if (i >= argc) {
				crips_help("You didn't give an initialization vector... wtf m8", ERROR_BAD_INITIALIZATION_VECTOR);
			}
			options->initialization_vector_provided = 1;

			// now parse the vector provided..
			// and validate argv[i] as good IV
			char delimiters[] = " ";
			char *result;
			int given_iv_length = strlen(argv[i]);
			int iv_index = 0;

			// strtok will modify the string so we cant give argv[i], it's const. need to copy it.
			char *given_iv_as_mutable_string = (char *)malloc(sizeof(char) * (given_iv_length + 1));
			memcpy(given_iv_as_mutable_string, argv[i], given_iv_length);
			result = strtok(given_iv_as_mutable_string, delimiters);
			while (result != NULL) {
				options->initialization_vector[iv_index++] = atoi(result);
				result = strtok(NULL, delimiters);
			}
			free(given_iv_as_mutable_string);
			if (iv_index != INITIALIZATION_VECTOR_LENGTH) {
				crips_help("You didn't give a proper initialization vector... read the rules", ERROR_BAD_INITIALIZATION_VECTOR);
			}
		}
		else if (strcmp("-r", argv[i]) == 0 || strcmp("--randomizer", argv[i]) == 0) {
			i++;
			if (i >= argc) {
				crips_help("You didn't give a randomizer... wtf m8", ERROR_BAD_RANDOMIZER);
			}
			if (strcmp(argv[i], "random") == 0) {
				options->random_source = MODE_RANDOM;
			}
			else if (strcmp(argv[i], "urandom") == 0) {
				options->random_source = MODE_URANDOM;
			}
			else {
				crips_help("You gave an invalid randomizer... only \"random\" and \"urandom\" are accepted", ERROR_BAD_RANDOMIZER);
			}
		}
		i++;
	}

	if (!mode_selected) {
		crips_help("You must select a mode", ERROR_MISSING_MODE);
	}
	if (!infile_selected) {
		crips_help("You must select an infile", ERROR_MISSING_INFILE);
	}
	if (!options->outfile_mode) {
		crips_help("You must select an outfile mode of either -o or -p", ERROR_MISSING_OUTFILE);
	}
	return options;
}


