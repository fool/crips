/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "file_ops.h"
#define BUFFER_SIZE 8192

/**
 * Checks if a file exists
 *
 * param filename The file to check
 * return 1 (true) if the file exists, 0 (false) otherwise
 */
int file_exists(const char *filename) {
	FILE *file;
	file = fopen(filename, "r");
	if (file) {
		fclose(file);
		return 1;
	}
	return 0;
}

/**
 * Writes a string to disk. It is possible for the write to fail
 * and the file still existing on disk in part or total. This will
 * always try to clean up the newly made file but it is not
 * guaranteed when an error occurs.
 *
 * param filename The file to write to
 * param write_mode The mode to open the file in
 * param contents The string to write
 * param contents_length The length of contents
 * return 1 (true) on success, 0 (false) otherwise
 */
int write_file(const char *filename, const char *write_mode, const char *contents, int contents_length) {
	FILE *file;
	file = fopen(filename, write_mode);
	if (!file) {
		return 0;
	}

	int bytes_written = fwrite(contents, 1, contents_length, file);

	if (bytes_written != contents_length) {
		fclose(file); // if this fails we cant do much.. already returning
		unlink(filename); // this too
		return 0;
	}

	fclose(file);

	return 1;
}

/**
 * Reads a file to string. You must free this memory afterwards
 *
 * param filename The file to read
 * return A malloc'd string, Null on errors.
 */
char *read_file_to_string(const char *filename) {

	FILE *file;
	file = fopen(filename, "r");
	if (!file) {
		return NULL;
	}
	char buffer[BUFFER_SIZE];
	int error;
	int bytes_read;
	int bytes_read_so_far = 0;
	char *contents;
	unsigned long file_size = (unsigned long)file_size_from_ptr(file);
	
	rewind(file);
	contents = malloc(sizeof(char) * (file_size + 1));
	if (!contents) {
		fclose(file);
		return NULL;
	}
	while (!feof(file)) {
		bytes_read = fread(buffer, sizeof(char), BUFFER_SIZE, file);
		error = ferror(file);
		if (error) {
			free(contents);
			fclose(file);
			return NULL;
		}
		memcpy(&contents[bytes_read_so_far], buffer, bytes_read);
		bytes_read_so_far += bytes_read;
	}
	contents[bytes_read_so_far] = '\0';
	fclose(file);
	return contents;
}

/**
 * Gets a file's size. This will move your pointer to the end of the file!
 *
 * param fp The file to get the size of
 * Return the file's size in bytes
 */
off_t file_size_from_ptr(FILE *fp) {
	fseek(fp, 0, SEEK_END);
    return ftell(fp);
}

/**
 * Reads two files and compare if they have the same contents. Errors
 * will be returned as 0 just like contents mismatch.
 *
 * param file1
 * param file2
 * return 1 (true) when the files are equal, 0 (false) otherwise
 */
int file_compare_equal(FILE *file1, FILE *file2)
{
	char buffer1[BUFFER_SIZE];
	char buffer2[BUFFER_SIZE];
	int error1;
	int error2;
	int bytes_read1;
	int bytes_read2;
	memset(buffer1, '\0', BUFFER_SIZE);
	memset(buffer2, '\0', BUFFER_SIZE);
	int i;

	while (!feof(file1)) {
		if (feof(file2)) {
			return 0;
		}

		bytes_read1 = fread(buffer1, sizeof(char), BUFFER_SIZE, file1);
		error1 = ferror(file1);
		if (error1) {
			perror("Error reading from file1: ");
			return 0;
		}
		bytes_read2 = fread(buffer2, sizeof(char), BUFFER_SIZE, file2);
		error2 = ferror(file2);
		if (error2) {
			perror("Error reading from file2: ");
			return 0;
		}
		if (bytes_read1 != bytes_read2) {
			return 0;
		}

		for (i = 0; i < BUFFER_SIZE; i++) {
			if (buffer1[i] != buffer2[i]) {
				return 0;
			}
		}

	}
	if (!feof(file2)) {
		return 0;
	}
	return 1;

}

/**
 * Convenience function to pass in file names and not have to deal with
 * opening and closing files. The downside to this is it will return
 * 0 if the contents did not match OR an error occurred such as file
 * not existing. You need to handle that yourself.
 *
 * param file1 The first file for comparison
 * param file2 The other file for comparison
 * return 1 (true) if they matched, 0 (false) otherwise
 */
int file_compare_equal_with_names(const char *file1, const char *file2) {
	FILE *file_ptr1 = fopen(file1, "rb");
	if (!file_ptr1) {
		return 0;
	}
	FILE *file_ptr2 = fopen(file2, "rb");

	if (!file_ptr2) {
		fclose(file_ptr1);
		return 0;
	}

	int result = file_compare_equal(file_ptr1, file_ptr2);
	fclose(file_ptr1);
	fclose(file_ptr2);
	return result;
}

/**
 * Gives you a string containing the cwd. This must be freed by you at some point.
 *
 * return The current cwd in a string
 */
char *get_cwd() {
	char *cwd = (char *)malloc(sizeof(char) * BUFFER_SIZE);

	char *result = getcwd(cwd, BUFFER_SIZE);

	if (result == NULL) {
		free(cwd);
		return NULL;
	}
	cwd = realloc(cwd, strlen(cwd));
	return cwd;	
}
