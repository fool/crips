/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "crips.h"
#include "help.h"
#include "crypt_interface.h"
#include "base36.h"
#include "string_ops.h"


/**
 * Runs the crips executable. This has the same signature as a main() function. It is called
 * from the crips main and other tests. See the crips_help() functions or run crips with no
 * arguments for info on what is accepted in the arguments
 *
 * TODO: DEPRECATED
 */
int crips_start_command_line(int argc, char **argv) {
	Crips_Options *options = crips_options_parse(argc, argv);
	int result = crips_start(options);
	free(options);
	return result;
}

/**
 * Main interface to the crips program. All requests go through here to be processed.
 *
 * param options The settings for this run on crips
 * return Exit code, 0 if successful
 */
int crips_start(Crips_Options *options) {
	crypt_init();
	char error_message[500];
	int exit_code = 0;

	switch (options->mode) {
		case MODE_ENCRYPT:
			crips_encrypt_file(options);
			break;
		case MODE_DECRYPT:
			exit_code = crips_decrypt_file(options);
			break;
		default:
			sprintf(error_message, "ERROR: Unknown mode: %d\n", options->mode);
			free(options);
			crips_help(error_message, ERROR_MISSING_MODE);
	}

	return exit_code;
}

/**
 * Encrypts a file.
 *
 * param infile The file to encrypt
 * param outfile_mode The way to store the result-> Either preserve mode or not.
 * param outfile The file to write to if we are not using preserve name mode.
 * param key The encryption key
 * param iv A provided initialization vector
 * param initialization_vector_provided Whether or not the "iv" argument should be use (one will be generated if not)
 * param verbose Whether or not we are logging verbosely
 */
void crips_encrypt_file(Crips_Options *options) {
	// open infile
	FILE *infile_ptr = fopen(options->infile, "rb");
	if (!infile_ptr) {
		fprintf(stderr, "ERROR: Un-readable in file: %s\n", options->infile);
		exit(ERROR_UNREADABLE_INFILE);
	}
	
	// get initiailization vector
	unsigned char initialization_vector[INITIALIZATION_VECTOR_LENGTH];
	if (options->initialization_vector_provided) {
		memcpy(initialization_vector, options->initialization_vector, INITIALIZATION_VECTOR_LENGTH);
	}
	else {
		int result = generate_initialization_vector(initialization_vector, INITIALIZATION_VECTOR_LENGTH, options->random_source);
		if (!result) {
			fprintf(stderr, "ERROR: Unable to use random source to generate an initialization vector, can't continue: %s\n", options->random_source);
			exit(ERROR_GENERATING_INITIALIZATION_VECTOR);
		}
	}
	
	if (options->verbose) {
 		printf("initialization vector is: ");
		print_unsigned_chars(initialization_vector, INITIALIZATION_VECTOR_LENGTH);
	 }

	// get the out file
	FILE *outfile_ptr;
	if (options->outfile_mode == OUTFILE_MODE_PRESERVE_NAME) {

		// copy the infile to unsigned char so it can be passed to the base36 method
		// without throwing a warning of signed-ness
		int infile_length = strlen(options->infile);
		unsigned char *infile_as_uchar = (unsigned char *)malloc(sizeof(char) * (infile_length + 1));
		memcpy(infile_as_uchar, options->infile, infile_length);

		infile_as_uchar[infile_length] = '\0';
		unsigned char *encrypted_filename = encrypt_or_decrypt_string(MODE_ENCRYPT, infile_as_uchar, infile_length, options->aes_key, initialization_vector);

		// base36 encode name. filename length is the same because encryption does not change length of contents
		char *encoded_filename = base_36_encode_uchar(encrypted_filename, infile_length);
		if (options->verbose) {
			printf("Encrypting %s to %s\n", options->infile, encoded_filename);
		}
		outfile_ptr = fopen(encoded_filename, "wb");
		if (!outfile_ptr) {
			fprintf(stderr, "ERROR: Un-writable out file: %s\n", encoded_filename);
			fclose(infile_ptr);
			free(encoded_filename);
			exit(ERROR_UNWRITABLE_OUTFILE);
		}
		free(encoded_filename);
	}
	else {
		if (options->verbose) {
			printf("Encrypting %s to %s\n", options->infile, options->outfile);
		}
		outfile_ptr = fopen(options->outfile, "wb");
		if (!outfile_ptr) {
			fprintf(stderr, "ERROR: Un-writable out file: %s\n", options->outfile);
			fclose(infile_ptr);
			exit(ERROR_UNWRITABLE_OUTFILE);
		}
	}

	// write empty bits to the front of the file. this is a placeholder for the hmac
	// which will be first. after the file is written with the cipher text we seek back
	// to the front to write the hmac.
	//code here....
	fwrite("00000000000000000000000000000000", 1, HMAC_LENGTH, outfile_ptr);
	// write iv to file
	fwrite(initialization_vector, 1, INITIALIZATION_VECTOR_LENGTH, outfile_ptr);
	// encrypt, get hmac
	unsigned char *hmac = encrypt_or_decrypt_file(MODE_ENCRYPT, infile_ptr, outfile_ptr, options->aes_key, options->hmac_key, initialization_vector);
	// write hmac to front of file
	rewind(outfile_ptr);
	fwrite(hmac, 1, HMAC_LENGTH, outfile_ptr);
	free(hmac);
	fclose(infile_ptr);
	fclose(outfile_ptr);
}

/**
 * Decrypts a file.
 *
 * param options A set of options
 * return int 0 if everything worked. Non-zero otherwise. This should be the exit code of the program.
 */
int crips_decrypt_file(Crips_Options *options) {
	int exit_code = 0;

	// check input args, does the file exist?
	FILE *infile_ptr = fopen(options->infile, "rb");
	if (!infile_ptr) {
		fprintf(stderr, "ERROR: Un-readable in file: %s\n", options->infile);
		exit(ERROR_UNREADABLE_INFILE);
	}
	
	FILE *outfile_ptr;
	unsigned char initialization_vector[INITIALIZATION_VECTOR_LENGTH];
	unsigned char hmac[HMAC_LENGTH];

	// 1. read the file and get the HMAC and IV
	if (!read_encrypted_file_metadata(infile_ptr, initialization_vector, hmac)) {
		fclose(infile_ptr);
		fprintf(stderr, "ERROR: In file does not have well formed contents: %s\n", options->infile);
		exit(ERROR_UNREADABLE_INFILE);
	}
	if (options->outfile_mode == OUTFILE_MODE_PRESERVE_NAME) {
		// decode and decrypt the filename, this must be freed later.
		int filename_length = strlen(options->infile);

		if (options->verbose) {
			printf("Read file and got this iv: ");
			print_unsigned_chars(initialization_vector, INITIALIZATION_VECTOR_LENGTH);
		}

		// 2. base36 decode the name
		unsigned char *decoded_filename = base_36_decode_uchar(options->infile, filename_length);
		int decoded_length = filename_length / 2;

		// 3. decrypt the name with the IV
		unsigned char *plaintext_filename_uchar = encrypt_or_decrypt_string(MODE_DECRYPT, decoded_filename, decoded_length, options->aes_key, initialization_vector);
		char *plaintext_filename = (char *) malloc(sizeof(char) *  (decoded_length + 1));
		memcpy(plaintext_filename, plaintext_filename_uchar, decoded_length);
		plaintext_filename[decoded_length] = '\0';
		
		if (options->verbose) {
			printf("Plaintext filename is: %s with length %d\n", plaintext_filename, decoded_length);
		}
		free(decoded_filename);

		// 4. open ptr to the decrypted filename
		if (options->verbose) {
			printf("Decrypting %s to %s\n", options->infile, plaintext_filename);
		}

		outfile_ptr = fopen(plaintext_filename, "wb");
		if (!outfile_ptr) {
			fprintf(stderr, "ERROR: Un-writable out file: %s\n", plaintext_filename);
			fclose(infile_ptr);
			free(plaintext_filename);
			exit(ERROR_UNWRITABLE_OUTFILE);
		}
		free(plaintext_filename); // now that ptr is open, free the string
	}
	else {
		if (options->verbose) {
			printf("Decrypting %s to %s\n", options->infile, options->outfile);
		}
		outfile_ptr = fopen(options->outfile, "wb");
		if (!outfile_ptr) {
			fprintf(stderr, "ERROR: Un-writable out file: %s\n", options->outfile);
			fclose(infile_ptr);
			exit(ERROR_UNWRITABLE_OUTFILE);
		}
	}

	unsigned char *computed_hmac = encrypt_or_decrypt_file(MODE_DECRYPT, infile_ptr, outfile_ptr, options->aes_key, options->hmac_key, initialization_vector);

	int match = 1;
	int i = 0;
	for (i = 0; i < HMAC_LENGTH; i++) {
		if (computed_hmac[i] != hmac[i]) {
			match = 0;
			break;
		}
	}
	if (!match) {
		fprintf(stderr, "ERROR: HMAC of the encrypted data did not match the original! Authenticity and integrity of decrypted contents is not guaranteed.\n");
		exit_code = ERROR_BAD_HMAC;
	}

	// ensure hmac == the original one in the file
	free(computed_hmac);
	fclose(infile_ptr);
	fclose(outfile_ptr);
	return exit_code;
}

