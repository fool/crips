/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include "string_ops.h"

/**
 * Helper function to print an array of unsigned chars. This should
 * probably be moved.
 *
 * param string The unsigned char array to print
 * param length How big the array is
 */
void print_unsigned_chars(const unsigned char *string, const int length) {
	int i = 0;
	for (i = 0; i < length; i++) {
		printf("%u.", string[i]);
	}
	printf("\n");
}

/**
 * Clones as char * string into an unsigned char * string.
 *
 * param string The string to clone
 * return pointer to the newly made string, you have to free this memory
 *
unsigned char *clone_as_uchar_string(const char *string) {
	int length = strlen(string);
	char *unsigned_clone = (unsigned char *)malloc(sizeof(unsigned char) * (length + 1));
	memcpy(unsigned_clone, string, length);
	unsigned_clone[length] = '\0';
	return unsigned_clone;
}*/
