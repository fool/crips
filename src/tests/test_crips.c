/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include "../crips.h"
#include "../crypt_interface.h"
#include "../file_ops.h"
#include "../system_ops.h"
#include "test_crips.h"

#define TEST_ENCRYPTION_KEY "42AB7FCE7BFEEE03E16719044916CBD475F6D000F230D213FF0F4775EF8D46F5"
#define TEST_HMAC_KEY "BC5E4B9BF20005092D2DA597151581AE348256443626244FC652C71E86D539E0"

/**
 * Tests for the crips program.
 * these are done from a total black box point of view.
 * 
 * you may want more lower level tests on just crypt interface
 */
int main(int argc, char **argv)
{
	int result = 0;
	result += test_file_encrypts_and_decrypts_to_same_contents_with_preserve_name();
	result += test_encrypt_and_decrypt_from_file();
	result += test_file_encrypts_and_decrypts_to_same_contents_with_random_iv();
	result += test_file_encrypts_and_decrypts_to_same_contents_with_random_iv_using_urandom();
	return result;
}

typedef struct {
	Crips_Options *encrypt_options;
	Crips_Options *decrypt_options;
	char *original_filename; // The starting filename
	char *expected_filename; // The filename after encryption
	char *temporary_copy_of_original_filename;
	int verbose;
} Test_Crips_Options;

/**
 * Runs crips with the specified options. All the tests ended up doing the same work,
 * so this function abstracts it out. It will run crips twice, first with encryption
 * options and again with decryption options. It will check for file correctness and clean
 * up for you. This is not guarnateed to clean up everything.
 *
 * param options The test options to run
 * return 1 on success, 0 on failure
 */
int do_crips_test(const Test_Crips_Options *options) {
	int failed = 0;
	int error; // used for system calls later

	// 1. ensure the output file doesn't already exist..
	if (file_exists(options->expected_filename)) {
		if (options->verbose) {
			printf("Encrypted test file already exists.. deleting %s\n", options->expected_filename);
		}
		error = unlink(options->expected_filename);
		if (error != 0) {
			fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), options->expected_filename);
			failed = 1;
			return failed;
		}
	}

	// 2. call crips with options
	crips_start(options->encrypt_options);

	// 3. check to see it created a file successfully
	if (!file_exists(options->expected_filename)) {
		fprintf(stderr, "%s:%d: Expected outfile file does not exist: %s\n", __FILE__, __LINE__, options->expected_filename);
		failed = 1;
		return failed;
	}

	// 4. rename original file to something else
	error = rename(options->original_filename, options->temporary_copy_of_original_filename);
	if (error != 0) {
		fprintf(stderr, "%s:%d: Tried to rename file but saw error %d, %s.\nOriginal name: %s. Destination: %s\n", __FILE__, __LINE__, error, strerror(error), options->original_filename, options->temporary_copy_of_original_filename);
		failed = 1;
		return failed;
	}

	// 5. call crips on that with decrypt options
	if (options->verbose) {
		printf("Decrypting file...\n");
	}
	crips_start(options->decrypt_options);
	
	// 6. make sure decoded filename matches and the original file is remade with original contents
	if (!file_compare_equal_with_names(options->original_filename, options->temporary_copy_of_original_filename)) {
		fprintf(stderr, "%s:%d: Decrypted contents did not match original.\n", __FILE__, __LINE__);
		failed = 1;
		return failed;
	}

	// 7. delete original file
	error = unlink(options->original_filename);
	if (error != 0) {
		fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), options->original_filename);
		failed = 1;
		// dont exit, still try to clean up the rest
	}
	
	// 8. delete encrypted file
	error = unlink(options->expected_filename);
	if (error != 0) {
		fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), options->expected_filename);
		failed = 1;
		// dont exit, still try to clean up the rest
	}

	// 9. undo temporary copy made in stop 4
	error = rename(options->temporary_copy_of_original_filename, options->original_filename);
	if (error != 0) {
		fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), options->temporary_copy_of_original_filename);
		failed = 1;
		// dont exit, still try to clean up the rest
	}

	return failed;
}

int test_file_encrypts_and_decrypts_to_same_contents_with_preserve_name()
{
	int failed = 0;
	int verbose = 0;
	int error; // used for system calls later

	// 1. create file. name, contents, initialization vector are kept constant to produce a constant output filename
	char *filename = "the god damn filename";
	const char *contents = "The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of darkness, for he is truly his brother's keeper and the finder of lost children. And I will strike down upon thee with great vengeance and furious anger those who attempt to poison and destroy my brothers. And you will know my name is the Lord when I lay my vengeance upon thee.";
	unsigned char initialization_vector[] = {30, 153, 45, 94, 145, 145, 198, 183, 246, 221, 197, 237, 108, 65, 12, 122};
	char *expected_filename = "532f532e4t5p5x0i242s383i2j5m3m4x1s3e3l0f23";

	// ensure the output file doesn't already exist..
	if (file_exists(expected_filename)) {
		if (verbose) {
			printf("Encrypted test file already exists.. deleting %s\n", expected_filename);
		}
		error = unlink(expected_filename);
		if (error != 0) {
			fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), expected_filename);
			failed = 1;
			return failed;
		}
	}

	// write input file with test contents
	if (verbose) {
		printf("Writing test cleartext contents to file: %s...\n", filename);
	}
	if (!write_file(filename, "w", contents, strlen(contents))) {
		fprintf(stderr, "%s:%d: Unable to create test file: %s", __FILE__, __LINE__, filename);
		failed = 1;
		return failed;
	}
	
	helper_for_test_encrypt_and_decrypt_from_file(filename, expected_filename, expected_filename, initialization_vector);

	// delete original file
	error = unlink(filename);
	if (error != 0) {
		fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), filename);
		failed = 1;
	}
	return failed;
}



/**
 * param filename The original plaintext file to encrypt
 * param expected_contents_filename A file that contains the expected encrypted contents
 * param encoded_encrypted_filename The output filename
 * param initialization_vector The IV
 */
int helper_for_test_encrypt_and_decrypt_from_file(char *filename, char *expected_contents_filename, char *encoded_encrypted_filename, unsigned char *initialization_vector) {

	Crips_Options encrypt_options;
	encrypt_options.mode = MODE_ENCRYPT;
	encrypt_options.aes_key = (unsigned char *) TEST_ENCRYPTION_KEY;
	encrypt_options.hmac_key = (unsigned char *) TEST_HMAC_KEY;
	encrypt_options.infile = filename;
	encrypt_options.outfile_mode = OUTFILE_MODE_PRESERVE_NAME;
	encrypt_options.initialization_vector_provided = 1;
	memcpy(encrypt_options.initialization_vector, initialization_vector, INITIALIZATION_VECTOR_LENGTH);
	encrypt_options.verbose = 0;
	
	Crips_Options decrypt_options;
	decrypt_options.mode = MODE_DECRYPT;
	decrypt_options.aes_key = (unsigned char *) TEST_ENCRYPTION_KEY;
	decrypt_options.hmac_key = (unsigned char *) TEST_HMAC_KEY;
	decrypt_options.infile = encoded_encrypted_filename;
	decrypt_options.outfile_mode = OUTFILE_MODE_PRESERVE_NAME;
	decrypt_options.verbose = 0;


	Test_Crips_Options options;
	options.encrypt_options = &encrypt_options;
	options.decrypt_options = &decrypt_options;
	options.original_filename = filename;
	options.expected_filename = encoded_encrypted_filename;
	options.temporary_copy_of_original_filename = "copy of file for test in progress";
	options.verbose = 0;

	int failed = do_crips_test(&options);

	return failed;
}

/**
 * tests large file encryption..
 * 2 pre computed files will be used.
 * these should stay constant
 */
int test_encrypt_and_decrypt_from_file() {
	int failed = 0;
	char *original_cwd = get_cwd();
	chdir("src/tests/testfiles");

	unsigned char initialization_vector1[] = {88, 113, 187, 226, 13, 155, 182, 17, 63, 122, 123, 114, 159, 72, 254, 145};
	failed += helper_for_test_encrypt_and_decrypt_from_file("Ulysses.txt", "Ulysses.txt.enc.cfb","6p5e2n266n5b163v616b5l", initialization_vector1);
	unsigned char initialization_vector2[] = {221, 132, 167, 39, 228, 245, 24, 255, 40, 150, 104, 41, 220, 206, 191, 220};
	failed += helper_for_test_encrypt_and_decrypt_from_file("Alice in Wonderland.txt", "Alice in Wonderland.txt.enc.cfb", "033l12335p4s4t2m1i5r0h18314g236f70096i491h1k6d", initialization_vector2);

	chdir(original_cwd);
	free(original_cwd);

	return failed;
}

/**
 * This is similar to the one above but does not use a constant IV, randomly generated
 */
int test_file_encrypts_and_decrypts_to_same_contents_with_random_iv()
{
	int failed = 0;
	int verbose = 0;
	int error; // used for system calls later

	// 1. create file. name, contents, initialization vector are kept constant to produce a constant output filename
	char *filename = "a differenter file";
	const char *contents = "The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of darkness, for he is truly his brother's keeper and the finder of lost children. And I will strike down upon thee with great vengeance and furious anger those who attempt to poison and destroy my brothers. And you will know my name is the Lord when I lay my vengeance upon thee.";
	char *expected_filename = "a differenter file.enc";

	// ensure the output file doesn't already exist..
	if (file_exists(expected_filename)) {
		if (verbose) {
			printf("Encrypted test file already exists.. deleting %s\n", expected_filename);
		}
		error = unlink(expected_filename);
		if (error != 0) {
			printf("%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), expected_filename);
			failed = 1;
			return failed;
		}
	}

	// write input file with test contents
	if (verbose) {
		printf("Writing test cleartext contents to file: %s...\n", filename);
	}
	if (!write_file(filename, "w", contents, strlen(contents))) {
		printf("%s:%d: Unable to create test file: %s", __FILE__, __LINE__, filename);
		failed = 1;
		return failed;
	}

	// 2. call crips on it with -e and -o
	Crips_Options encrypt_options;
	encrypt_options.mode = MODE_ENCRYPT;
	encrypt_options.aes_key = (unsigned char *)TEST_ENCRYPTION_KEY;
	encrypt_options.hmac_key = (unsigned char *) TEST_HMAC_KEY;
	encrypt_options.infile = filename;
	encrypt_options.outfile_mode = OUTFILE_MODE_NAMED_FILE;
	encrypt_options.outfile = expected_filename;
	encrypt_options.verbose = 0;
	encrypt_options.random_source = MODE_RANDOM;
	
	Crips_Options decrypt_options;
	decrypt_options.mode = MODE_DECRYPT;
	decrypt_options.aes_key = (unsigned char *)TEST_ENCRYPTION_KEY;
	decrypt_options.hmac_key = (unsigned char *) TEST_HMAC_KEY;
	decrypt_options.infile = expected_filename;
	decrypt_options.outfile_mode = OUTFILE_MODE_NAMED_FILE;
	decrypt_options.outfile = filename;
	decrypt_options.verbose = 0;

	Test_Crips_Options options;
	options.encrypt_options = &encrypt_options;
	options.decrypt_options = &decrypt_options;
	options.original_filename = filename;
	options.expected_filename = expected_filename;
	options.temporary_copy_of_original_filename = "copy of file for test in progress";
	options.verbose = 0;

	failed = do_crips_test(&options);

	// delete original file
	error = unlink(filename);
	if (error != 0) {
		fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), filename);
		failed = 1;
	}

	return failed;
}

/**
 * This is similar to the one above but does not use a constant IV, randomly generated with /dev/urandom
 */
int test_file_encrypts_and_decrypts_to_same_contents_with_random_iv_using_urandom()
{
	int failed = 0;
	int verbose = 0;
	int error; // used for system calls later

	// 1. create file. name, contents, initialization vector are kept constant to produce a constant output filename
	char *filename = "a different file";
	//const char *temporary_copy_of_original = "a different file.copy";
	const char *contents = "The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of darkness, for he is truly his brother's keeper and the finder of lost children. And I will strike down upon thee with great vengeance and furious anger those who attempt to poison and destroy my brothers. And you will know my name is the Lord when I lay my vengeance upon thee.";
	char *expected_filename = "a different file.enc";

	// ensure the output file doesn't already exist..
	if (file_exists(expected_filename)) {
		if (verbose) {
			printf("Encrypted test file already exists.. deleting %s\n", expected_filename);
		}
		error = unlink(expected_filename);
		if (error != 0) {
			printf("%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), expected_filename);
			failed = 1;
			return failed;
		}
	}

	// write input file with test contents
	if (verbose) {
		printf("Writing test cleartext contents to file: %s...\n", filename);
	}
	if (!write_file(filename, "w", contents, strlen(contents))) {
		printf("%s:%d: Unable to create test file: %s", __FILE__, __LINE__, filename);
		failed = 1;
		return failed;
	}

	// 2. call crips on it with -e and -o
	Crips_Options encrypt_options;
	encrypt_options.mode = MODE_ENCRYPT;
	encrypt_options.aes_key = (unsigned char *)TEST_ENCRYPTION_KEY;
	encrypt_options.hmac_key = (unsigned char *) TEST_HMAC_KEY;
	encrypt_options.infile = filename;
	encrypt_options.outfile_mode = OUTFILE_MODE_NAMED_FILE;
	encrypt_options.outfile = expected_filename;
	encrypt_options.random_source = MODE_URANDOM;
	encrypt_options.verbose = 0;
	
	Crips_Options decrypt_options;
	decrypt_options.mode = MODE_DECRYPT;
	decrypt_options.aes_key = (unsigned char *)TEST_ENCRYPTION_KEY;
	decrypt_options.hmac_key = (unsigned char *) TEST_HMAC_KEY;
	decrypt_options.infile = expected_filename;
	decrypt_options.outfile_mode = OUTFILE_MODE_NAMED_FILE;
	decrypt_options.outfile = filename;
	decrypt_options.verbose = 0;

	Test_Crips_Options options;
	options.encrypt_options = &encrypt_options;
	options.decrypt_options = &decrypt_options;
	options.original_filename = filename;
	options.expected_filename = expected_filename;
	options.temporary_copy_of_original_filename = "copy of file for test in progress";
	options.verbose = 0;

	failed = do_crips_test(&options);

	// delete original file
	error = unlink(filename);
	if (error != 0) {
		fprintf(stderr, "%s:%d: Tried to delete file but saw error %d, %s. File: %s\n", __FILE__, __LINE__, error, strerror(error), filename);
		failed = 1;
	}

	return failed;
}
