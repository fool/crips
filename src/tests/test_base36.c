/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../base36.h"
#include "test_base36.h"
#include "../file_ops.h"


/**
 * Tests for the base36 code
 */
int main(int argc, char **argv)
{
	// tests return 0 if pass, 1 if fail. if this is still 0 at the end of all
	// tests then it was ok.
	int result = 0;
	result += test_encode_and_decode_int();
	result += test_encode_and_decode_char();
	result += test_encode_int_and_decode_char();
	result += test_encode_char_and_decode_int();
	result += test_encode_and_decode_int_known_values();
	result += test_encode_and_decode_char_known_values();
	result += test_decode_to_int_only_accepts_strings_with_length_multiple_of_eight();
	result += test_decode_uint_ignores_numbers_over_uint_max();
	result += test_encode_and_decode_char_from_file();
	return result;
}


/**
 * Int to int encoding and decoding
 */
int test_encode_and_decode_int()
{
	int length = 5;
	int i;
	int failed = 0; // set to 1 if anything fails
	unsigned int *test_values_to_encode = (unsigned int *) malloc(sizeof(unsigned int) * length);
	for (i = 0; i < length; i++) {
		test_values_to_encode[i] = i * i * i * i;
	}

	char *encoded_values = base_36_encode_uint(test_values_to_encode, length);

	unsigned int *decoded_values = base_36_decode_uint(encoded_values, strlen(encoded_values));

	for (i = 0; i < length; i++) {
		if (decoded_values[i] != test_values_to_encode[i]) {
			printf("%s:%d: ERROR Decoded value at index %d does not match original value. Original: %u Decoded: %u\n", __FILE__, __LINE__, i, test_values_to_encode[i], decoded_values[i]);
			failed = 1;
		}
	}
	free(encoded_values);
	free(decoded_values);
	free(test_values_to_encode);
	return failed;
}

/**
 * Char to char encoding and decoding
 */
int test_encode_and_decode_char()
{
	unsigned char *test_values_to_encode;
	int length = 5;
	int failed = 0;
	int i;
	test_values_to_encode = (unsigned char *) malloc(sizeof(unsigned char) * length);
	for (i = 0; i < length; i++) {
		test_values_to_encode[i] = (i * 23) + 10;
	}
	char *encoded_values = base_36_encode_uchar(test_values_to_encode, length);

	unsigned char *decoded_values = base_36_decode_uchar(encoded_values, strlen(encoded_values));

	for (i = 0; i < length; i++) {
		if (decoded_values[i] != test_values_to_encode[i]) {
			printf("%s:%d: ERROR Decoded value at index %d does not match original value. Original: %u Decoded: %u\n", __FILE__, __LINE__, i, test_values_to_encode[i], decoded_values[i]);
			failed = 1;
		}
	}
	free(encoded_values);
	free(decoded_values);
	free(test_values_to_encode);
	return failed;
}

/**
 * cross compatibility going from the int encoded to the char decoder
 */
int test_encode_int_and_decode_char()
{
	unsigned int *test_values_to_encode;
	int length = 5;
	int failed = 0;
	int i;
	test_values_to_encode = (unsigned int *) malloc(sizeof(unsigned char) * length);
	for (i = 0; i < length; i++) {
		test_values_to_encode[i] = (i + 1) * 2; // encoding: 2, 4, 6, 8, 10
	}

	// should look like: "00000002000000040000000800000010"
	char *encoded_values = base_36_encode_uint(test_values_to_encode, length);

	// the original number of integers is 5. there are 8 encoded chars per inter, so the encoded length was 40 chars
	// When decoding to unsigned chars, 2 chars of base36 map to one char goes to one unsigned char, so the resulting
	// decode will have 40/2 = 20 characters
	int decoded_length = 20;
	unsigned char expected_decoded_value[20] = {
		0, 0, 0, 2,
		0, 0, 0, 4,
		0, 0, 0, 6,
		0, 0, 0, 8,
		0, 0, 0, 10,
	};

	unsigned char *decoded_values = base_36_decode_uchar(encoded_values, strlen(encoded_values));

	for (i = 0; i < decoded_length; i++) {
		if (decoded_values[i] != expected_decoded_value[i]) {
			printf("%s:%d: ERROR Decoded value at index %d does not match original value. Original: %d Decoded: %d\n", __FILE__, __LINE__, i, expected_decoded_value[i], decoded_values[i]);
			failed = 1;
		}
	}
	free(encoded_values);
	free(decoded_values);
	free(test_values_to_encode);
	return failed;
}

/**
 * cross compatibility going from the char encoder to the int decoder
 */
int test_encode_char_and_decode_int()
{
	unsigned char *test_values_to_encode;
	int length = 5;
	int failed = 0;
	int i;
	test_values_to_encode = (unsigned char *) malloc(sizeof(unsigned char) * length);
	for (i = 0; i < length; i++) {
		test_values_to_encode[i] = ((i + 1) * 2) - 1; // encoding: 1, 3, 5, 7, 01
		if (i == 4) {
			test_values_to_encode[i] = 1;
		}
	}
	// will look like: "0103050701"
	char *encoded_values = base_36_encode_uchar(test_values_to_encode, length);

	// must pad encoded values with 6 zeros because int decoding needs 8 chars per int.
	// a 10 char string is invalid, must be multiple of 8. pad to 16.
	int pad_length = 6;
	int encoded_length = strlen(encoded_values) + (pad_length * sizeof(char));
	encoded_values = (char *) realloc(encoded_values, encoded_length + 1);
	memset(&encoded_values[10], '0', pad_length);
	encoded_values[encoded_length] = '\0';


	// 1, 3, 5, 7 are added to each octet on the first int
	// 01 is added to the first octet of the 2nd int
	unsigned int expected_decoded_value[2] = {
		2181827671, // decimal vers of 01030507
		2176782336  // decimal vers of 01000000
	};

	unsigned int *decoded_values = base_36_decode_uint(encoded_values, encoded_length);
	if (decoded_values == NULL)
	{
		printf("%s:%d: ERROR Unable to decode value at all: %s\n", __FILE__, __LINE__, encoded_values);
		failed = 1;
		return failed;
	}

	int decoded_length = 2;
	for (i = 0; i < decoded_length; i++) {
		if (decoded_values[i] != expected_decoded_value[i]) {
			printf("%s:%d: ERROR Decoded value at index %d does not match original value. Original: %u Decoded: %u\n", __FILE__, __LINE__, i, expected_decoded_value[i], decoded_values[i]);
			failed = 1;
		}
	}

	free(encoded_values);
	free(decoded_values);
	free(test_values_to_encode);
	return failed;
}

/**
 * It is an invalid string to decode to ints if the length is not evenly divisible by 8
 */
int test_decode_to_int_only_accepts_strings_with_length_multiple_of_eight() {
	char *bad_input = (char *)malloc(sizeof(char) * 4);
	int failed = 0;
	memcpy(bad_input, "not8", 4);
	
	unsigned int *decoded_values = base_36_decode_uint(bad_input, 4);

	if (decoded_values != NULL) {
		printf("%s:%d: ERROR A string with length that is not evenly divisble by 8 should not be accepted and null should be returned", __FILE__, __LINE__);
		failed = 1;
	}
	return failed;
}

/**
 * test a bunch of random numbers. small, big, really big.
 * a lot of copy and paste from the char test just because...
 */
int test_encode_and_decode_int_known_values() {
	int i;
	char *encoded_value;
	int failed = 0;
	unsigned int *decoded_value;
	int num_tests = 264;
	unsigned int values_to_encode[264] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
	                                      31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58,
	                                      59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86,
	                                      87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
	                                      112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
	                                      135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157,
	                                      158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180,
	                                      181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203,
	                                      204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226,
	                                      227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249,
	                                      250, 251, 252, 253, 254, 255, 1025, 65537, 32923145, 9023412, 940123129, 2147483647, 2147483646, 23442345
	};

	char expected_encoded_value[264][8] = {"00000000", "00000001", "00000002", "00000003", "00000004", "00000005", "00000006", "00000007", "00000008", "00000009",
	                                       "0000000a", "0000000b", "0000000c", "0000000d", "0000000e", "0000000f", "0000000g", "0000000h", "0000000i",
	                                       "0000000j", "0000000k", "0000000l", "0000000m", "0000000n", "0000000o", "0000000p", "0000000q", "0000000r",
	                                       "0000000s", "0000000t", "0000000u", "0000000v", "0000000w", "0000000x", "0000000y", "0000000z", "00000010",
	                                       "00000011", "00000012", "00000013", "00000014", "00000015", "00000016", "00000017", "00000018", "00000019",
	                                       "0000001a", "0000001b", "0000001c", "0000001d", "0000001e", "0000001f", "0000001g", "0000001h", "0000001i",
	                                       "0000001j", "0000001k", "0000001l", "0000001m", "0000001n", "0000001o", "0000001p", "0000001q", "0000001r",
	                                       "0000001s", "0000001t", "0000001u", "0000001v", "0000001w", "0000001x", "0000001y", "0000001z", "00000020",
	                                       "00000021", "00000022", "00000023", "00000024", "00000025", "00000026", "00000027", "00000028", "00000029",
	                                       "0000002a", "0000002b", "0000002c", "0000002d", "0000002e", "0000002f", "0000002g", "0000002h", "0000002i",
	                                       "0000002j", "0000002k", "0000002l", "0000002m", "0000002n", "0000002o", "0000002p", "0000002q", "0000002r",
	                                       "0000002s", "0000002t", "0000002u", "0000002v", "0000002w", "0000002x", "0000002y", "0000002z", "00000030",
	                                       "00000031", "00000032", "00000033", "00000034", "00000035", "00000036", "00000037", "00000038", "00000039",
	                                       "0000003a", "0000003b", "0000003c", "0000003d", "0000003e", "0000003f", "0000003g", "0000003h", "0000003i",
										   "0000003j", "0000003k", "0000003l", "0000003m", "0000003n", "0000003o", "0000003p", "0000003q", "0000003r",
	                                       "0000003s", "0000003t", "0000003u", "0000003v", "0000003w", "0000003x", "0000003y", "0000003z", "00000040",
	                                       "00000041", "00000042", "00000043", "00000044", "00000045", "00000046", "00000047", "00000048", "00000049",
	                                       "0000004a", "0000004b", "0000004c", "0000004d", "0000004e", "0000004f", "0000004g", "0000004h", "0000004i",
	                                       "0000004j", "0000004k", "0000004l", "0000004m", "0000004n", "0000004o", "0000004p", "0000004q", "0000004r",
	                                       "0000004s", "0000004t", "0000004u", "0000004v", "0000004w", "0000004x", "0000004y", "0000004z", "00000050",
	                                       "00000051", "00000052", "00000053", "00000054", "00000055", "00000056", "00000057", "00000058", "00000059",
	                                       "0000005a", "0000005b", "0000005c", "0000005d", "0000005e", "0000005f", "0000005g", "0000005h", "0000005i",
	                                       "0000005j", "0000005k", "0000005l", "0000005m", "0000005n", "0000005o", "0000005p", "0000005q", "0000005r",
	                                       "0000005s", "0000005t", "0000005u", "0000005v", "0000005w", "0000005x", "0000005y", "0000005z", "00000060",
	                                       "00000061", "00000062", "00000063", "00000064", "00000065", "00000066", "00000067", "00000068", "00000069",
	                                       "0000006a", "0000006b", "0000006c", "0000006d", "0000006e", "0000006f", "0000006g", "0000006h", "0000006i",
	                                       "0000006j", "0000006k", "0000006l", "0000006m", "0000006n", "0000006o", "0000006p", "0000006q", "0000006r",
	                                       "0000006s", "0000006t", "0000006u", "0000006v", "0000006w", "0000006x", "0000006y", "0000006z", "00000070",
	                                       "00000071", "00000072", "00000073", "000000sh", "00001ekh", "000jlnnt", "0005deic", "00fjq3nd", "00zik0zj",
	                                       "00zik0zi", "000dyg89"
	};

	// encode to base36, should match expected, decode it back to decimal, should match original
	for (i = 0; i < num_tests; i++) {
		encoded_value = base_36_encode_uint(&values_to_encode[i], 1);
		if (encoded_value[0] != expected_encoded_value[i][0] && encoded_value[1] != expected_encoded_value[i][1] &&
		    encoded_value[2] != expected_encoded_value[i][2] && encoded_value[3] != expected_encoded_value[i][3] &&
		    encoded_value[4] != expected_encoded_value[i][4] && encoded_value[5] != expected_encoded_value[i][5] &&
		    encoded_value[6] != expected_encoded_value[i][6] && encoded_value[7] != expected_encoded_value[i][7]) {
			failed = 1;
			printf("%s:%d: ERROR Encoding %d to base 36 differs from expected.\nExpected: %.2s\nSaw: %s\n", __FILE__, __LINE__, values_to_encode[i], expected_encoded_value[i], encoded_value);
		}
		decoded_value = base_36_decode_uint(encoded_value, strlen(encoded_value));
		if (*decoded_value != values_to_encode[i]) {
			failed = 1;
			printf("%s:%d: ERROR Decoding %s from base 36 differs from Expected.\nExpected value: %d\nSaw: %d\n", __FILE__, __LINE__, encoded_value, values_to_encode[i], *decoded_value);
		}
	}
	free(encoded_value);
	free(decoded_value);
	return failed;
	return 0;
}

/**
 * test every possible char value against a set of known results
 * tests every number from 0 to 255
 */
int test_encode_and_decode_char_known_values() {
	int i;
	char *encoded_value;
	int failed = 0;
	unsigned char *decoded_value;
	unsigned char values_to_encode[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
	                                      31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58,
	                                      59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86,
	                                      87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
	                                      112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
	                                      135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157,
	                                      158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180,
	                                      181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203,
	                                      204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226,
	                                      227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249,
	                                      250, 251, 252, 253, 254, 255};
	char expected_encoded_value[][2] = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", "0g", "0h", "0i",
	                                      "0j", "0k", "0l", "0m", "0n", "0o", "0p", "0q", "0r", "0s", "0t", "0u", "0v", "0w", "0x", "0y", "0z", "10", "11",
	                                      "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f", "1g", "1h", "1i", "1j", "1k",
	                                      "1l", "1m", "1n", "1o", "1p", "1q", "1r", "1s", "1t", "1u", "1v", "1w", "1x", "1y", "1z", "20", "21", "22", "23",
	                                      "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f", "2g", "2h", "2i", "2j", "2k", "2l", "2m",
	                                      "2n", "2o", "2p", "2q", "2r", "2s", "2t", "2u", "2v", "2w", "2x", "2y", "2z", "30", "31", "32", "33", "34", "35",
	                                      "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", "3g", "3h", "3i", "3j", "3k", "3l", "3m", "3n", "3o",
	                                      "3p", "3q", "3r", "3s", "3t", "3u", "3v", "3w", "3x", "3y", "3z", "40", "41", "42", "43", "44", "45", "46", "47",
	                                      "48", "49", "4a", "4b", "4c", "4d", "4e", "4f", "4g", "4h", "4i", "4j", "4k", "4l", "4m", "4n", "4o", "4p", "4q",
	                                      "4r", "4s", "4t", "4u", "4v", "4w", "4x", "4y", "4z", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
	                                      "5a", "5b", "5c", "5d", "5e", "5f", "5g", "5h", "5i", "5j", "5k", "5l", "5m", "5n", "5o", "5p", "5q", "5r", "5s",
	                                      "5t", "5u", "5v", "5w", "5x", "5y", "5z", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b",
	                                      "6c", "6d", "6e", "6f", "6g", "6h", "6i", "6j", "6k", "6l", "6m", "6n", "6o", "6p", "6q", "6r", "6s", "6t", "6u",
	                                      "6v", "6w", "6x", "6y", "6z", "70", "71", "72", "73"};
	for (i = 0; i < sizeof(values_to_encode); i++) {
		encoded_value = base_36_encode_uchar(&values_to_encode[i], 1);
		if (encoded_value[0] != expected_encoded_value[i][0] && encoded_value[1] != expected_encoded_value[i][1]) {
			failed = 1;
			printf("%s:%d: ERROR Encoding %d to base36 differs from expected\nExpected: %.2s\nSaw: %s\n", __FILE__, __LINE__, values_to_encode[i], expected_encoded_value[i], encoded_value);
		}
		decoded_value = base_36_decode_uchar(encoded_value, strlen(encoded_value));
		if (*decoded_value != values_to_encode[i]) {
			failed = 1;
			printf("%s:%d: ERROR Decoding %s from base36 differs from expected\nExpected: %d\nSaw: %d\n", __FILE__, __LINE__, encoded_value, values_to_encode[i], *decoded_value);
		}
	}
	free(encoded_value);
	free(decoded_value);
	return failed;
}

/**
 * The 8 characters that represent one encoded unsigned integer of base36
 * contain more possible combinations than base36 representations.
 *
 * The largest number is base36(UINT_MAX). Anything over that should
 * be rejected. This will create input that is too large and it
 * should all be rejected.
 */
int test_decode_uint_ignores_numbers_over_uint_max() {
	int number_of_tests = 8;
	int test;
	int failed = 0;
	char tests[8][8] = {
		"11z141z3",
		"21z141z3",
		"01z241z3",
		"01z151z3",
		"01z142z3",
		"01z141z4",
		"02z141z3",
		"0zzzzzz3"
	};

	for (test = 0; test < number_of_tests; test++) {
		unsigned int *decoded_value = base_36_decode_uint(tests[test], 8);
		if (decoded_value != NULL) {
			printf("%s:%d ERROR (Test %d of %d) Value over UINT_MAX was not rejected: %s\n", __FILE__, __LINE__, test, number_of_tests, tests[test]);
			failed = 1;
		}
	}

	return failed;
}

int test_encode_and_decode_char_from_file() {
	int failed = 0;
	failed += helper_for_test_encode_and_decode_char_from_file("src/tests/testfiles/Ulysses.txt", "src/tests/testfiles/Ulysses.txt.base36");
	failed += helper_for_test_encode_and_decode_char_from_file("src/tests/testfiles/Alice in Wonderland.txt", "src/tests/testfiles/Alice in Wonderland.txt.base36");
	return failed;
}

/**
 * This doesnt do the sexiest and most optimal stuff but its
 * easy to read.
 */
int helper_for_test_encode_and_decode_char_from_file(const char *filename, const char *expected_filename) {
	int failed = 0;
	
	// 1. ensure input and expected output files exist
	if (!file_exists(filename)) {
		printf("%s:%d: File doesn't exist: %s\n", __FILE__, __LINE__, filename);
		failed = 1;
		return failed;
	}
	if (!file_exists(expected_filename)) {
		printf("%s:%d: File doesn't exist: %s\n", __FILE__, __LINE__, expected_filename);
		failed = 1;
		return failed;
	}

	// 2. read file to string
	char *file_contents = read_file_to_string(filename);
	if (!file_contents) {
		printf("%s:%d: Unable to read file: %s\n", __FILE__, __LINE__, filename);
		failed = 1;
		return failed;
	}

	// 3. encode original
	unsigned char *unsigned_file_contents = (unsigned char *)file_contents;
	char *encoded_contents = base_36_encode_uchar(unsigned_file_contents, strlen(file_contents));

	// 4. read expected output to string, compare to what we got
	char *expected_encoded_contents = read_file_to_string(expected_filename);
	if (strcmp(encoded_contents, expected_encoded_contents) != 0) {
		printf("%s:%d: Files did not match. Compared:\nCleartext file: %s\nEncoded file: %s\n", __FILE__, __LINE__, filename, expected_filename);
		failed = 1;
		free(encoded_contents);
		free(expected_encoded_contents);
		free(file_contents);
		return failed;
	}

	// all good
	free(expected_encoded_contents);

	// decode and compare to original
	unsigned char *decoded_contents = base_36_decode_uchar(encoded_contents, strlen(encoded_contents));
	free(encoded_contents);

	// strcmp wants a char, not unsigned char
	char *decoded_contents_as_char = (char *)decoded_contents;
	if (strcmp(decoded_contents_as_char, file_contents) != 0) {
		printf("%s:%d: Contents did not match after decoding\n", __FILE__, __LINE__);
		failed = 1;
		free(file_contents);
		free(decoded_contents);
		return failed;
	}

	free(file_contents);
	free(decoded_contents);
	return failed;
}

int test_encode_and_decode_uint_from_file() {
	return 0;
}

